<?php

namespace App\Repository;

use App\Entity\MenuItem;
use App\Entity\Reservation;
use DateTime;
use PDO;

class ReservationRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = Database::connect();
    }

    public function findAll()
    {
        $reservation = [];

        $statment = $this->connection->prepare('SELECT * FROM reservation');
        $statment->execute();



        foreach ($statment->fetchAll() as $item) {
            $reservation[] = new Reservation($item['nom'], $item['prenom'], $item['numero'], $item['nbpersonnes'], new DateTime($item['date']), new DateTime($item['heure']), $item['id']);
        }

        return $reservation;
    }


    public function findById(int $id): ?Reservation
    {
        $statement = $this->connection->prepare('SELECT * FROM reservation WHERE id=:id');
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if ($result) {
            return $this->sqlToPost($result);
        }
        return null;
    }

    public function sqlToPost(array $item): Reservation
    {

        return new Reservation($item['nom'], $item['prenom'], $item['numero'], $item['nbpersonnes'], new DateTime($item['date']), new DateTime($item['heure']), $item['id']);
    }
    public function persist(Reservation $reservation)
    {

        $statement = $this->connection->prepare('INSERT INTO reservation(nom, prenom, numero, nbpersonnes, date, heure) VALUES (:nom, :prenom, :numero, :nbpersonnes, :date, :heure)');

        $statement->execute([
            'nom' => $reservation->getNom(),
            'prenom' => $reservation->getPrenom(),
            'numero' => $reservation->getNumero(),
            'nbpersonnes' => $reservation->getNbpersonnes(),
            'date' => $reservation->getDate()->format('Y-m-d'),
            'heure' => $reservation->getHeure()->format('Y-m-d')
        ]);
        $reservation->setId($this->connection->lastInsertId());
    }

    public function update(Reservation $reservation): void
    {
        $query = $this->connection->prepare("UPDATE reservation SET nom=:nom, prenom=:prenom, numero=:numero, nbpersonnes=:nbpersonnes, date=:date, heure=:heure WHERE id=:id");

        $query->bindValue(":nom", $reservation->getNom(), PDO::PARAM_STR);
        $query->bindValue(":prenom", $reservation->getPrenom(), PDO::PARAM_STR);
        $query->bindValue(":numero", $reservation->getNumero(), PDO::PARAM_INT);
        $query->bindValue(":nbpersonnes", $reservation->getNbpersonnes(), PDO::PARAM_INT);
        $query->bindValue(":date", $reservation->getDate()->format('Y-m-d'));
        $query->bindValue(":heure", $reservation->getHeure()->format('Y-m-d'));
        $query->bindValue(":id", $reservation->getId(), PDO::PARAM_INT);

        $query->execute();
    }

    public function deleteById(int $id)
    {
        $statement = $this->connection->prepare("DELETE FROM reservation WHERE id=:id");
        $statement->bindValue('id', $id);

        $statement->execute();

    }


    public function findByReservationId($idReservation)
    {
        
        $reservation = null;

        $statment = $this->connection->prepare(' SELECT reservation.nom, reservation.prenom, reservation.numero, reservation.nbpersonnes, reservation.date, reservation.heure, reservation.id,
        menuitem.titre, menuitem.description, menuitem.prix, menuitem.categorie, menuitem.image, menuitem.id
        FROM reservation
        LEFT JOIN reservation_item ON reservation.id = reservation_item.reservation_id
        LEFT JOIN menuitem ON reservation_item.menuitem_id = menuitem.id
        WHERE reservation.id =:id');

$statment->bindValue(":id", $idReservation, PDO::PARAM_INT);
        $statment->execute();
        

        foreach ($statment->fetchAll() as $item) {
            if(!$reservation){

                $reservation = new Reservation($item['nom'], $item['prenom'], $item['numero'], $item['nbpersonnes'], new DateTime($item['date']), new DateTime($item['heure']), $item['id']);
            } 
            $menuitem = new MenuItem($item['titre'], $item['description'], $item['prix'], $item['categorie'], $item['image'], $item['id']);

            $reservation->addMenuItem($menuitem);
            
        }
        return $reservation;

    }
}