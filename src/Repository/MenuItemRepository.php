<?php

namespace App\Repository;
use App\Entity\MenuItem;
use DateTime;
use PDO;

class MenuItemRepository{
    private PDO $connection;
    
    public function __construct()
    {
        $this->connection = Database::connect();
    }

    public function findAll(){
        $menuitem = [];

        $statment = $this->connection->prepare('SELECT * FROM menuitem');
        $statment->execute();

        

        foreach($statment->fetchAll() as $item){
            $menuitem[] = new Menuitem($item['titre'], $item['description'], $item['prix'], $item['categorie'], $item['image'], $item['id']);
        }
        
        return $menuitem;
    }


    public function findById(int $id):?Menuitem {
        $statement = $this->connection->prepare('SELECT * FROM menuitem WHERE id=:id');
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToPost($result);
        }
        return null;
    }

    public function sqlToPost(array $item):MenuItem {
       
        return new Menuitem($item['titre'], $item['description'], $item['prix'], $item['categorie'], $item['image'], $item['id']);
    }
    public function persist(Menuitem $menuitem){
        
        $statement = $this->connection->prepare('INSERT INTO menuitem(titre, description, prix, categorie, image) VALUES (:titre, :description, :prix, :categorie, :image)');

        $statement->execute([
            'titre'  => $menuitem->getTitre(),
            'description'  => $menuitem->getDescription(),
            'prix' => $menuitem->getPrix(),
            'categorie'  => $menuitem->getCategorie(),
            'image'  => $menuitem->getImage(),
        ]);
        $menuitem->setId($this->connection->lastInsertId());
    }

    public function update(MenuItem $menuitem): void
    {
        $query = $this->connection->prepare("UPDATE menuitem SET titre=:titre, description=:description, prix=:prix, categorie=:categorie, image=:image WHERE id=:id");
        
        $query->bindValue(":titre", $menuitem->getTitre(), PDO::PARAM_STR);
        $query->bindValue(":description", $menuitem->getDescription(), PDO::PARAM_STR);
        $query->bindValue(":prix", $menuitem->getPrix(), PDO::PARAM_INT);
        $query->bindValue(":categorie", $menuitem->getCategorie(), PDO::PARAM_INT);
        $query->bindValue(":image", $menuitem->getImage());
        $query->bindValue(":id", $menuitem->getId(), PDO::PARAM_INT);
        
        $query->execute();
    }

    public function deleteById(int $id)
    {
        $statement = $this->connection->prepare("DELETE FROM menuitem WHERE id=:id");
        $statement->bindValue('id', $id);
        
        $statement->execute();
        
    }
}