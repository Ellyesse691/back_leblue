<?php

namespace App\Repository;
use App\Entity\Restaurant;
use PDO;

class RestaurantRepository{
    private PDO $connection;
    
    public function __construct()
    {
        $this->connection = Database::connect();
    }

    public function findAll(){
        $restaurant = [];

        $statment = $this->connection->prepare('SELECT * FROM restaurant');
        $statment->execute();

        

        foreach($statment->fetchAll() as $item){
            $restaurant[] = new Restaurant($item['nom'], $item['capacite'], $item['adresse'], $item['mail'], $item['portable'], $item['image'], $item['id']);
        }
        
        return $restaurant;
    }


    public function findById(int $id):?Restaurant {
        $statement = $this->connection->prepare('SELECT * FROM restaurant WHERE id=:id');
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToPost($result);
        }
        return null;
    }

    public function sqlToPost(array $item):Restaurant {
       
        return new Restaurant($item['nom'], $item['capacite'], $item['adresse'], $item['mail'], $item['portable'], $item['image'], $item['id']);
    }
    public function persist(Restaurant $restaurant){
        
        $statement = $this->connection->prepare('INSERT INTO restaurant(nom, capacite, adresse, mail, portable, image) VALUES (:nom, :capacite, :adresse, :mail, :portable, :image)');

        $statement->execute([
            'nom'  => $restaurant->getNom(),
            'capacite'  => $restaurant->getCapacite(),
            'adresse' => $restaurant->getAdresse(),
            'mail'  => $restaurant->getMail(),
            'portable'  => $restaurant->getPortable(),
            'image'  => $restaurant->getImage()
        ]);
        $restaurant->setId($this->connection->lastInsertId());
    }

    public function update(Restaurant $restaurant): void
    {
        $query = $this->connection->prepare("UPDATE restaurant SET nom=:nom, capacite=:capacite, adresse=:adresse, mail=:mail, portable=:portable, image=:image WHERE id=:id");
        
        $query->bindValue(":nom", $restaurant->getNom(), PDO::PARAM_STR);
        $query->bindValue(":capacite", $restaurant->getCapacite(), PDO::PARAM_INT);
        $query->bindValue(":adresse", $restaurant->getAdresse(), PDO::PARAM_STR);
        $query->bindValue(":mail", $restaurant->getMail(), PDO::PARAM_STR);
        $query->bindValue(":portable", $restaurant->getPortable(), PDO::PARAM_INT);
        $query->bindValue(":image", $restaurant->getImage(), PDO::PARAM_STR);
        $query->bindValue(":id", $restaurant->getId(), PDO::PARAM_INT);
        
        $query->execute();
    }

    public function deleteById(int $id)
    {
        $statement = $this->connection->prepare("DELETE FROM restaurant WHERE id=:id");
        $statement->bindValue('id', $id);
        
        $statement->execute();
        
    }
}