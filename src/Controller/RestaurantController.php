<?php

namespace App\Controller;
use App\Entity\Restaurant;
use App\Repository\RestaurantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;


#[Route('/api/restaurant')]
class RestaurantController extends AbstractController {
    private RestaurantRepository $repo;

    public function __construct(RestaurantRepository $repo) {
    	$this->repo = $repo;
    }

    #[Route(methods: 'GET')]
    public function findAll() {
        
        $restaurant = $this->repo->findAll();
        return $this->json($restaurant);
    }

    #[Route('/{id}' , methods: 'GET')]
    public function one(int $id) {
        $restaurant = $this->repo->findById($id);
        if(!$restaurant){
            throw new NotFoundHttpException();

        }
        return $this->json($restaurant);
    }

    #[Route('/{id}' , methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer) {
        $restaurant = $serializer->deserialize($request->getContent(), Restaurant::class, 'json');
        $this->repo->persist($restaurant);

        return $this->json($restaurant , Response::HTTP_CREATED);
    }

    #[Route('/{id}' , methods: 'DELETE')]
    public function deleteById(Request $request, SerializerInterface $serializer, int $id) {
        $restaurant = $this->repo->findById($id);

        if (!$restaurant) {
            $this->json(['message' => 'dog not found'], Response::HTTP_NO_CONTENT);
        }

        $this->repo->deleteById($restaurant);
        return $this->json($restaurant);
    }

}