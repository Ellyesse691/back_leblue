<?php

namespace App\Controller;
use App\Entity\Reservation;
use App\Repository\ReservationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;


#[Route('/api/reservation')]
class ReservationController extends AbstractController {
    private ReservationRepository $repo;

    public function __construct(ReservationRepository $repo) {
    	$this->repo = $repo;
    }

    #[Route(methods: 'GET')]
    public function findAll() {
        
        $reservation = $this->repo->findAll();
        return $this->json($reservation);
    }

    #[Route('/{id}' , methods: 'GET')]
    public function findById(int $id) {
        $reservation = $this->repo->findById($id);
        if(!$reservation){
            throw new NotFoundHttpException();

        }
        return $this->json($reservation);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer) {
        $reservation = $serializer->deserialize($request->getContent(), Reservation::class, 'json');
        $this->repo->persist($reservation);

        return $this->json($reservation , Response::HTTP_CREATED);
    }

    #[Route('/{id}' , methods: 'DELETE')]
    public function deleteById(Request $request, SerializerInterface $serializer, int $id) {
        $reservation = $this->repo->findById($id);

        if (!$reservation) {
            $this->json(['message' => 'dog not found'], Response::HTTP_NO_CONTENT);
        }

        $this->repo->deleteById($id);
        return $this->json($reservation);
    }

    #[Route('/client/{id}' , methods: 'GET')]
    public function findByReservationId(int $id) {
        $reservation = $this->repo->findByReservationId($id);
        if(!$reservation){
            throw new NotFoundHttpException();

        }
        return $this->json($reservation);
    }


}