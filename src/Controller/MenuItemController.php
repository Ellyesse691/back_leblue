<?php

namespace App\Controller;
use App\Entity\MenuItem;
use App\Repository\MenuItemRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;


#[Route('/api/menuitem')]
class MenuItemController extends AbstractController {
    private MenuItemRepository $repo;

    public function __construct(MenuItemRepository $repo) {
    	$this->repo = $repo;
    }

    #[Route(methods: 'GET')]
    public function findAll() {
        
        $menuitem = $this->repo->findAll();
        return $this->json($menuitem);
    }

    #[Route('/{id}' , methods: 'GET')]
    public function findById(int $id) {
        $menuitem = $this->repo->findById($id);
        if(!$menuitem){
            throw new NotFoundHttpException();

        }
        return $this->json($menuitem);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer) {
        $menuitem = $serializer->deserialize($request->getContent(), MenuItem::class, 'json');
        $this->repo->persist($menuitem);

        return $this->json($menuitem , Response::HTTP_CREATED);
    }

    #[Route('/{id}' , methods: 'DELETE')]
    public function deleteById(Request $request, SerializerInterface $serializer, int $id) {
        $menuitem = $this->repo->findById($id);

        if (!$menuitem) {
            $this->json(['message' => 'dog not found'], Response::HTTP_NO_CONTENT);
        }

        $this->repo->deleteById($id);
        return $this->json($menuitem);
    }

}