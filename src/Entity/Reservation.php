<?php

namespace App\Entity;
use DateTime;

class Reservation{
    private ?int $id;
    private ?string $nom;
    private ?string $prenom;
    private ?int $numero;
    private ?int $nbpersonnes;
    private ?DateTime $date;
    private ?DateTime $heure;
	private array $menuitem = [];

    public function __construct(?string $nom, ?string $prenom, ?int $numero, ?int $nbpersonnes, ?DateTime $date, ?DateTime $heure, ?int $id) {
    	$this->id = $id;
    	$this->nom = $nom;
    	$this->prenom = $prenom;
    	$this->numero = $numero;
        $this->nbpersonnes = $nbpersonnes;
        $this->date = $date;
        $this->heure = $heure;
    }


	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getNom(): ?string {
		return $this->nom;
	}
	
	/**
	 * @param  $nom 
	 * @return self
	 */
	public function setNom(?string $nom): self {
		$this->nom = $nom;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getPrenom(): ?string {
		return $this->prenom;
	}
	
	/**
	 * @param  $prenom 
	 * @return self
	 */
	public function setPrenom(?string $prenom): self {
		$this->prenom = $prenom;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getNumero(): ?int {
		return $this->numero;
	}
	
	/**
	 * @param  $numero 
	 * @return self
	 */
	public function setNumero(?int $numero): self {
		$this->numero = $numero;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getNbpersonnes(): ?int {
		return $this->nbpersonnes;
	}
	
	/**
	 * @param  $nbpersonnes 
	 * @return self
	 */
	public function setNbpersonnes(?int $nbpersonnes): self {
		$this->nbpersonnes = $nbpersonnes;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param  $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getHeure(): ?DateTime {
		return $this->heure;
	}
	
	/**
	 * @param  $heure 
	 * @return self
	 */
	public function setHeure(?DateTime $heure): self {
		$this->heure = $heure;
		return $this;
	}
	

	/**
	 * @return 
	 */
	public function getMenuitem(): ?array {
		return $this->menuitem;
	}
	

	public function addMenuItem(MenuItem $item){
		$this->menuitem[] = $item;
	}

	/**
	 * @param  $menuitem 
	 * @return self
	 */
	public function setMenuitem(?array $menuitem): self {
		$this->menuitem = $menuitem;
		return $this;
	}
}