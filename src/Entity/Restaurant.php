<?php

namespace App\Entity;


class Restaurant {
    private ?int $id;
    private ?string $nom;
    private ?int $capacite;
    private ?string $adresse;
    private ?string $mail;
    private ?int $portable;
    private ?string $image;


    public function __construct(?string $nom, ?int $capacite, ?string $adresse, ?string $mail, ?string $portable, ?string $image, ?int $id) {
    	$this->id = $id;
    	$this->nom = $nom;
    	$this->capacite = $capacite;
    	$this->adresse = $adresse;
        $this->mail = $mail;
        $this->portable = $portable;
        $this->image = $image;
    }

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getNom(): ?string {
		return $this->nom;
	}
	
	/**
	 * @param  $nom 
	 * @return self
	 */
	public function setNom(?string $nom): self {
		$this->nom = $nom;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getCapacite(): ?int {
		return $this->capacite;
	}
	
	/**
	 * @param  $capacite 
	 * @return self
	 */
	public function setCapacite(?int $capacite): self {
		$this->capacite = $capacite;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getAdresse(): ?string {
		return $this->adresse;
	}
	
	/**
	 * @param  $adresse 
	 * @return self
	 */
	public function setAdresse(?string $adresse): self {
		$this->adresse = $adresse;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getMail(): ?string {
		return $this->mail;
	}
	
	/**
	 * @param  $mail 
	 * @return self
	 */
	public function setMail(?string $mail): self {
		$this->mail = $mail;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getPortable(): ?int {
		return $this->portable;
	}
	
	/**
	 * @param  $portable 
	 * @return self
	 */
	public function setPortable(?int $portable): self {
		$this->portable = $portable;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getImage(): ?string {
		return $this->image;
	}
	
	/**
	 * @param  $image 
	 * @return self
	 */
	public function setImage(?string $image): self {
		$this->image = $image;
		return $this;
	}
}