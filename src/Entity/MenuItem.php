<?php

namespace App\Entity;


class MenuItem{
    private ?int $id;
    private ?string $titre;
    private ?string $description;
    private ?int $prix;
    private ?string $categorie;
    private ?string $image;


    public function __construct(?string $titre, ?string $description, ?int $prix, ?string $categorie, ?string $image, ?int $id) {
    	$this->id = $id;
    	$this->titre = $titre;
    	$this->description = $description;
    	$this->prix = $prix;
        $this->categorie = $categorie;
        $this->image = $image;
    }


	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getTitre(): ?string {
		return $this->titre;
	}
	
	/**
	 * @param  $titre 
	 * @return self
	 */
	public function setTitre(?string $titre): self {
		$this->titre = $titre;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getDescription(): ?string {
		return $this->description;
	}
	
	/**
	 * @param  $description 
	 * @return self
	 */
	public function setDescription(?string $description): self {
		$this->description = $description;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getPrix(): ?int {
		return $this->prix;
	}
	
	/**
	 * @param  $prix 
	 * @return self
	 */
	public function setPrix(?int $prix): self {
		$this->prix = $prix;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getCategorie(): ?string {
		return $this->categorie;
	}
	
	/**
	 * @param  $categorie 
	 * @return self
	 */
	public function setCategorie(?string $categorie): self {
		$this->categorie = $categorie;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getImage(): ?string {
		return $this->image;
	}
	
	/**
	 * @param  $image 
	 * @return self
	 */
	public function setImage(?string $image): self {
		$this->image = $image;
		return $this;
	}
}